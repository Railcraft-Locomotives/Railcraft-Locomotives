/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.gui.containers;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.common.gui.containers.RailcraftContainer;
import mods.railcraft.common.gui.slots.SlotUntouchable;
import mods.railcraft_loco.common.carts.EntityTender;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;


public class ContainerTender extends RailcraftContainer {

    private final EntityTender tender;
    private final InventoryPlayer playerInv;
    private int lastLockState;
    private final int guiHeight;
    public String ownerName;

    ContainerTender(InventoryPlayer playerInv, EntityTender tender, int guiHeight) {
        super(tender);
        this.tender = tender;
        this.playerInv = playerInv;
        this.guiHeight = guiHeight;
    }

    public static ContainerTender make(InventoryPlayer playerInv, EntityTender tender) {
        ContainerTender con = new ContainerTender(playerInv, tender, 161);
        con.init();
        return con;
    }

    public final void init() {
        defineSlotsAndWidgets();

        // TODO: make some way to clear this?
        addSlot(new SlotUntouchable(tender, tender.getSizeInventory() - 1, 134, guiHeight - 111));

        for (int i = 0; i < 3; i++) {
            for (int k = 0; k < 9; k++) {
                addSlot(new Slot(playerInv, k + i * 9 + 9, 8 + k * 18, guiHeight - 82 + i * 18));
            }
        }

        for (int j = 0; j < 9; j++) {
            addSlot(new Slot(playerInv, j, 8 + j * 18, guiHeight - 24));
        }
    }

    public void defineSlotsAndWidgets() {

    }

//    @Override
//    public void addListener(IContainerListener listener) {
//        super.addListener(listener);
//
//        listener.sendProgressBarUpdate(this, 12, tender.getLockController().getCurrentState());
//        listener.sendProgressBarUpdate(this, 13, PlayerPlugin.isOwnerOrOp(tender.getOwner(), playerInv.player) ? 1 : 0);
//
//        String oName = tender.getOwner().getName();
//        if (oName != null)
//            PacketBuilder.instance().sendGuiStringPacket(listener, windowId, 0, oName);
//    }

    @Override
    public void sendUpdateToClient() {
        super.sendUpdateToClient();

        for (Object crafter : crafters) {
        	ICrafting var2 = (ICrafting) crafter;
            /*LocoSpeed speed = loco.getSpeed();
            if (lastSpeed != speed)
                crafter.sendProgressBarUpdate(this, 10, speed.ordinal());

            LocoMode mode = loco.getMode();
            if (lastMode != mode)
                crafter.sendProgressBarUpdate(this, 11, mode.ordinal());*/

            int lock = tender.getLockController().getCurrentState();
            if (lastLockState != lock)
            	var2.sendProgressBarUpdate(this, 12, lock);
        }

        /*this.lastSpeed = loco.getSpeed();
        this.lastMode = loco.getMode();*/
        this.lastLockState = tender.getLockController().getCurrentState();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int id, int value) {
        switch (id) {
            /*case 10:
                loco.clientSpeed = LocoSpeed.VALUES[value];
                break;
            case 11:
                loco.clientMode = LocoMode.VALUES[value];
                break;*/
            case 12:
                tender.getLockController().setCurrentState(value);
                break;
            case 13:
                tender.clientCanLock = value == 1;
                break;
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateString(byte id, String data) {
        switch (id) {
            case 0:
                ownerName = data;
                break;
        }
    }

}