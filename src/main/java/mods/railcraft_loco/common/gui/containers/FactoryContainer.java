/* 
 * Copyright (c) CovertJaguar, 2014 http://railcraft.info
 * 
 * This code is the property of CovertJaguar
 * and may only be used with explicit written
 * permission unless otherwise specified on the
 * license page at http://railcraft.info/wiki/info:license.
 */
package mods.railcraft_loco.common.gui.containers;

import org.apache.logging.log4j.Level;
import mods.railcraft.common.blocks.detector.TileDetector;
import net.minecraft.inventory.Container;
import net.minecraft.entity.player.InventoryPlayer;
import mods.railcraft.common.blocks.machine.ITankTile;
import mods.railcraft.common.blocks.machine.TileMultiBlock;
import mods.railcraft.common.blocks.machine.alpha.*;
import mods.railcraft.common.blocks.machine.beta.TileBoilerFireboxFluid;
import mods.railcraft.common.blocks.machine.beta.TileBoilerFireboxSolid;
import mods.railcraft.common.blocks.machine.beta.TileEngineSteam;
import mods.railcraft.common.blocks.machine.beta.TileEngineSteamHobby;
import mods.railcraft.common.blocks.machine.gamma.*;
import mods.railcraft.common.blocks.signals.IAspectActionManager;
import mods.railcraft.common.blocks.signals.IRouter;
import mods.railcraft.common.blocks.tracks.TileTrack;
import mods.railcraft.common.blocks.tracks.TrackRouting;
import mods.railcraft.common.carts.*;
import mods.railcraft_loco.common.carts.EntityTenderSolid;
import mods.railcraft_loco.common.gui.EnumGui;
import mods.railcraft_loco.common.modules.ModuleManager;
import mods.railcraft.common.util.misc.Game;
import net.minecraft.world.World;

/**
 * @author CovertJaguar <http://www.railcraft.info>
 */
public class FactoryContainer {

    @SuppressWarnings("ConstantConditions")
    public static Container build(EnumGui gui, InventoryPlayer inv, Object obj, World world, int x, int y, int z) {
//        if (gui != EnumGui.ANVIL && obj == null)
//            return null;

        if (obj instanceof TileMultiBlock && !((TileMultiBlock) obj).isStructureValid())
            return null;

        try {
            switch (gui) {
                case TENDER_DUST:
                    return ContainerTenderSolid.make(inv, (EntityTenderSolid) obj);
                case TENDER_LIQUID:
                    return ContainerTenderSolid.make(inv, (EntityTenderSolid) obj);
                case TENDER_SOLID:
                    return ContainerTenderSolid.make(inv, (EntityTenderSolid) obj);
                default:
                    return ModuleManager.getGuiContainer(gui, inv, obj, world, x, y, z);
            }
        } catch (ClassCastException ex) {
            Game.log(Level.WARN, "Error when attempting to build gui container {0}: {1}", gui, ex);
        }
        return null;
    }

}
