/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2017
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.gui.containers;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft_loco.common.carts.EntityTenderSolid;
import mods.railcraft.common.gui.slots.SlotFuel;
import mods.railcraft.common.gui.slots.SlotOutput;
import mods.railcraft.common.gui.widgets.FluidGaugeWidget;
import mods.railcraft.common.gui.widgets.IndicatorWidget;
import net.minecraft.entity.player.InventoryPlayer;



public class ContainerTenderSolid extends ContainerTender {

    private final EntityTenderSolid tender;

    private ContainerTenderSolid(InventoryPlayer playerInv, EntityTenderSolid tender) {
        super(playerInv, tender, 205);
        this.tender = tender;
    }

    public static ContainerTenderSolid make(InventoryPlayer playerInv, EntityTenderSolid tender) {
        ContainerTenderSolid con = new ContainerTenderSolid(playerInv, tender);
        con.init();
        return con;
    }

    @Override
    public void defineSlotsAndWidgets() {
        addWidget(new FluidGaugeWidget(tender.getTankManager().get(0), 53, 23, 176, 0, 16, 47));
        addWidget(new FluidGaugeWidget(tender.getTankManager().get(1), 17, 23, 176, 0, 16, 47));

        addSlot(new SlotOutput(tender, 1, 152, 56));
        addSlot(new SlotOutput(tender, 2, 116, 56));
        addSlot(new SlotFuel(tender, 3, 116, 20));
        addSlot(new SlotFuel(tender, 4, 80, 20));
        addSlot(new SlotFuel(tender, 5, 80, 38));
        addSlot(new SlotFuel(tender, 6, 80, 56));
    }

    @Override
    public void sendUpdateToClient() {
        super.sendUpdateToClient();
        /*for (IContainerListener listener : listeners) {
            if (lastBurnTime != tender.boiler.burnTime)
                listener.sendProgressBarUpdate(this, 20, (int) Math.round(tender.boiler.burnTime));

            if (lastItemBurnTime != tender.boiler.currentItemBurnTime)
                listener.sendProgressBarUpdate(this, 21, (int) Math.round(tender.boiler.currentItemBurnTime));
        }

        this.lastBurnTime = tender.boiler.burnTime;
        this.lastItemBurnTime = tender.boiler.currentItemBurnTime;*/
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int id, int value) {
        super.updateProgressBar(id, value);

        /*switch (id) {
            case 20:
                loco.boiler.burnTime = value;
                break;
            case 21:
                loco.boiler.currentItemBurnTime = value;
                break;
        }*/
    }

}