/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2017
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.gui;

/**
 * @author CovertJaguar
 */
public enum EnumGui {

    TENDER_DUST(true),
	TENDER_LIQUID(true),
	TENDER_SOLID(false);
    private static final EnumGui[] VALUES = values();
    private final boolean hasContainer;

    EnumGui(boolean hasContainer) {
        this.hasContainer = hasContainer;
    }

    public boolean hasContainer() {
        return hasContainer;
    }

    public static EnumGui fromOrdinal(int i) {
        if (i < 0 || i >= VALUES.length)
            return null;
        return VALUES[i];
    }

}