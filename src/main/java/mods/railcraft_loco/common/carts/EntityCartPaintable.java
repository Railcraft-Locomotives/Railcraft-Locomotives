/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info

 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.carts;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mojang.authlib.GameProfile;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import mods.railcraft.api.carts.CartTools;
import mods.railcraft.api.carts.ILinkableCart;
import mods.railcraft.api.carts.IMinecart;
import mods.railcraft.api.carts.IPaintedCart;
import mods.railcraft.api.carts.IRoutableCart;
import mods.railcraft.api.carts.locomotive.LocomotiveRenderType;
import mods.railcraft_loco.api.carts.tender.TenderRenderType;
import mods.railcraft.common.carts.CartConstants;
import mods.railcraft.common.carts.CartContainerBase;
import mods.railcraft.common.carts.EnumCart;
import mods.railcraft.common.carts.ItemLocomotive;
import mods.railcraft.common.carts.LinkageManager;
import mods.railcraft.common.plugins.forge.LocalizationPlugin;
import mods.railcraft.common.util.misc.EnumColor;
import mods.railcraft.common.util.misc.MiscTools;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public abstract class EntityCartPaintable extends CartContainerBase implements ILinkableCart, IMinecart, IPaintedCart {
    private static final byte PRIMARY_COLOR_DATA_ID = 25;
    private static final byte SECONDARY_COLOR_DATA_ID = 26;
    private static final byte EMBLEM_DATA_ID = 29;

    protected float renderYaw;
    protected String model = "";

    public EntityCartPaintable(World world) {
        super(world);
        setPrimaryColor(EnumColor.LIGHT_GRAY.ordinal());
        setSecondaryColor(EnumColor.GRAY.ordinal());
    }

    public EntityCartPaintable(World world, double d, double d1, double d2) {
        this(world);
        setPosition(d, d1 + (double) yOffset, d2);
//        motionX = 0.0D;
//        motionY = 0.0D;
//        motionZ = 0.0D;
        prevPosX = d;
        prevPosY = d1;
        prevPosZ = d2;
    }
    
    @Override
    protected void entityInit() {
        super.entityInit();
        
        dataWatcher.addObject(PRIMARY_COLOR_DATA_ID, (byte) 0);
        dataWatcher.addObject(SECONDARY_COLOR_DATA_ID, (byte) 0);
        dataWatcher.addObject(EMBLEM_DATA_ID, "");
    }
    
    public void initEnitityFromItem(ItemStack item) {
    	NBTTagCompound nbt = item.getTagCompound();
    	if (nbt == null)
    		return;
    	
    	setPrimaryColor(ItemTender.getPrimaryColor(item).ordinal());
    	setSecondaryColor(ItemTender.getSecondaryColor(item).ordinal());
    	if (nbt.hasKey("emblem"))
    		setEmblem(nbt.getString("emblebm"));
    	if (nbt.hasKey("model"))
    		model = nbt.getString("model");
    }
    
    @Override
    public boolean doesCartMatchFilter(ItemStack stack, EntityMinecart cart) {
        return EnumCart.getCartType(stack) == getCartType();
    }

    public String getName() {
        return LocalizationPlugin.translate(getLocalizationTag());
    }
    
    public String getLocalizationTag() {
        return getCartType().getTag();
    }

    public GameProfile getOwner() {
        return CartTools.getCartOwner(this);
    }

    public double getDrag() {
    	return CartConstants.STANDARD_DRAG;
    }
    
    @Override
    public List<ItemStack> getItemsDropped() {
        List<ItemStack> items = new ArrayList<ItemStack>();
        items.add(getCartItem());
        return items;
    }

    @Override
    public ItemStack getCartItem() {
        ItemStack item = getCartItemBase();
        
        ItemLocomotive.setItemColorData(item, getPrimaryColor(), getSecondaryColor());
        ItemLocomotive.setModel(item, getModel());
        ItemLocomotive.setEmblem(item, getEmblem());
        if (hasCustomInventoryName())
            item.setStackDisplayName(getCommandSenderName());
        return item;
    }

    protected abstract ItemStack getCartItemBase();
    
    protected abstract void openGui(EntityPlayer player);
    

    public String getEmblem() {
        return dataWatcher.getWatchableObjectString(EMBLEM_DATA_ID);
    }

    public void setEmblem(String emblem) {
        if (!getEmblem().equals(emblem))
            dataWatcher.updateObject(EMBLEM_DATA_ID, emblem);
    }

    public void reverse() {
        rotationYaw += 180;
        motionX = -motionX;
        motionZ = -motionZ;
    }

    public void setRenderYaw(float yaw) {
        renderYaw = yaw;
    }
            
    public void writeEntityToNBT(NBTTagCompound data) {
        super.writeEntityToNBT(data);

        data.setString("model", model);

        data.setString("emblem", getEmblem());

        data.setByte("primaryColor", getPrimaryColor());
        data.setByte("secondaryColor", getSecondaryColor());

    }

    @Override
    public void readEntityFromNBT(NBTTagCompound data) {
        super.readEntityFromNBT(data);

        ObfuscationReflectionHelper.setPrivateValue(EntityMinecart.class, this, data.getBoolean("isInReverse"), 0);

        model = data.getString("model");

        setEmblem(data.getString("emblem"));

        setPrimaryColor(data.getByte("primaryColor"));
        setSecondaryColor(data.getByte("secondaryColor"));
    }

    public void writeSpawnData(ByteBuf data) {
    	try {
    		DataOutputStream byteStream = new DataOutputStream(new ByteBufOutputStream(data));
            byteStream.writeUTF(func_95999_t() != null ? func_95999_t() : "");
            byteStream.writeUTF(model);
            byteStream.close();
        } catch (IOException ex) {
		}
    }

    public void readSpawnData(ByteBuf data) {
        try {
            DataInputStream byteStream = new DataInputStream(new ByteBufInputStream(data));
            String name = byteStream.readUTF();
            if (!name.equals(""))
                setMinecartName(name);
            model = byteStream.readUTF();
            byteStream.close();
        } catch (IOException ex) {
        }
    }

    @Override
    public float getLinkageDistance(EntityMinecart cart) {
        return LinkageManager.LINKAGE_DISTANCE;
    }

    @Override
    public float getOptimalDistance(EntityMinecart cart) {
        return 0.9f;
    }

    public abstract TenderRenderType getRenderType();

    public byte getPrimaryColor() {
        return dataWatcher.getWatchableObjectByte(PRIMARY_COLOR_DATA_ID);
    }

    public final void setPrimaryColor(int color) {
        dataWatcher.updateObject(PRIMARY_COLOR_DATA_ID, (byte) color);
    }

    public byte getSecondaryColor() {
        return dataWatcher.getWatchableObjectByte(SECONDARY_COLOR_DATA_ID);
    }

    public final void setSecondaryColor(int color) {
        dataWatcher.updateObject(SECONDARY_COLOR_DATA_ID, (byte) color);
    }

    public final String getModel() {
        return model;
    }

    public final void setModel(String model) {
        this.model = model;
    }
}
