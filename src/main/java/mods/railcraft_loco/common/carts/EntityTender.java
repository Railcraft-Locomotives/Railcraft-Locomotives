/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info

 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.carts;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.mojang.authlib.GameProfile;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import mods.railcraft.api.carts.CartTools;
import mods.railcraft.api.carts.IFluidCart;
import mods.railcraft.api.carts.ILinkableCart;
import mods.railcraft.api.carts.IMinecart;
import mods.railcraft.api.carts.IRoutableCart;
import mods.railcraft.api.carts.tender.TenderRenderType;
import mods.railcraft.common.blocks.signals.ISecure;
import mods.railcraft.common.carts.EntityLocomotive;
import mods.railcraft_loco.common.carts.EntityTender.LockButtonState;
import mods.railcraft.common.carts.EnumCart;
import mods.railcraft.common.carts.ICartType;
import mods.railcraft.common.carts.IDirectionalCart;
import mods.railcraft.common.carts.ItemLocomotive;
import mods.railcraft.common.carts.LinkageManager;
import mods.railcraft.common.fluids.FluidHelper;
import mods.railcraft.common.fluids.FluidItemHelper;
import mods.railcraft.common.fluids.Fluids;
import mods.railcraft.common.fluids.TankManager;
import mods.railcraft.common.fluids.tanks.FilteredTank;
import mods.railcraft.common.fluids.tanks.StandardTank;
import mods.railcraft_loco.common.gui.EnumGui;
import mods.railcraft.common.gui.buttons.ButtonTextureSet;
import mods.railcraft.common.gui.buttons.IButtonTextureSet;
import mods.railcraft.common.gui.buttons.IMultiButtonState;
import mods.railcraft.common.gui.buttons.MultiButtonController;
import mods.railcraft.common.gui.tooltips.ToolTip;
import mods.railcraft.common.items.ItemWhistleTuner;
import mods.railcraft.common.plugins.forge.LocalizationPlugin;
import mods.railcraft.common.plugins.forge.PlayerPlugin;
import mods.railcraft.common.util.inventory.InvTools;
import mods.railcraft.common.util.inventory.wrappers.InventoryMapper;
import mods.railcraft.common.util.misc.Game;
import mods.railcraft.common.util.network.IGuiReturnHandler;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;

public abstract class EntityTender extends EntityCartPaintable implements IDirectionalCart, IEntityAdditionalSpawnData, IFluidCart, IFluidHandler, IGuiReturnHandler, ILinkableCart, IMinecart, IRoutableCart, ISecure<LockButtonState> {
    protected static final byte SLOT_LIQUID_INPUT = 0;
    private static final byte SLOT_LIQUID_OUTPUT = 1;
    private static final byte FILLING_DATA_ID = 28;
    private static final int TANK_WATER = 0;
    private final MultiButtonController<LockButtonState> lockController = new MultiButtonController(0, LockButtonState.VALUES);
    public boolean clientCanLock;
    protected StandardTank tankWater;
    protected InventoryMapper invWaterInput;
    protected IInventory invWaterOutput = new InventoryMapper(this, SLOT_LIQUID_OUTPUT, 1);
    private TankManager tankManager;
    private int update = rand.nextInt();

    public EntityTender(World world) {
        super(world);
    }

    public EntityTender(World world, double x, double y, double z) {
    	super(world, x, y, z);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        
        tankManager = new TankManager();
        
        tankWater = new FilteredTank(FluidHelper.BUCKET_VOLUME * 6, Fluids.WATER.get());
        
        tankManager.add(tankWater);
        
        invWaterInput = new InventoryMapper(this, SLOT_LIQUID_INPUT, 1);
        invWaterInput.setStackSizeLimit(4);
    }
    
    @Override
    public void initEnitityFromItem(ItemStack item) {
    	super.initEnitityFromItem(item);
    	NBTTagCompound nbt = item.getTagCompound();
    	if (nbt == null)
    		return;
    	
    	if (nbt.hasKey("owner")) {
    		CartTools.setCartOwner(this, PlayerPlugin.readOwnerFromNBT(nbt));
    		setSecurityState(LockButtonState.LOCKED);
    	}
    	if (nbt.hasKey("security"))
    		setSecurityState(LockButtonState.VALUES[nbt.getByte("security")]);
    }

    public TankManager getTankManager() {
        return tankManager;
    }
    
    
	@Override
    public void onUpdate() {
        super.onUpdate();
	    update++;
	    
        if (Game.isHost(worldObj)) {
      		if (update % FluidHelper.BUCKET_FILL_TIME == 0)
      			FluidHelper.drainContainers(this, this, SLOT_LIQUID_INPUT, SLOT_LIQUID_OUTPUT);
        }
    }
	
    @Override
    public void readEntityFromNBT(NBTTagCompound data) {
        super.readEntityFromNBT(data);

        tankManager.readTanksFromNBT(data);
        
        ObfuscationReflectionHelper.setPrivateValue(EntityMinecart.class, this, data.getBoolean("isInReverse"), 0);

        lockController.readFromNBT(data, "lock");
    }
	
    @Override
    public void writeEntityToNBT(NBTTagCompound data) {
        super.writeEntityToNBT(data);

        tankManager.writeTanksToNBT(data);
        
        Boolean isInReverse = ObfuscationReflectionHelper.getPrivateValue(EntityMinecart.class, this, 0);

        data.setBoolean("isInReverse", isInReverse);


        lockController.writeToNBT(data, "lock");
    }

    /** Interface methods of IDirectionalCart */

    @Override
    public void reverse() {
        rotationYaw += 180;
        motionX = -motionX;
        motionZ = -motionZ;
    }

    @Override
    public void setRenderYaw(float yaw) {
        renderYaw = yaw;
    }
    
    /** Interface methods of IEntityAdditionalSpawnData */
    
    @Override
    public void writeSpawnData(ByteBuf data) {
    	DataOutputStream byteStream = null;
        try {
			byteStream = new DataOutputStream(new ByteBufOutputStream(data));
            byteStream.writeUTF(func_95999_t() != null ? func_95999_t() : "");
            byteStream.writeUTF(model);
        } catch (IOException ex) {
        } finally {
        	if (byteStream != null)
	        	try {
	        		byteStream.close();
	        	} catch (Exception e) {}
        }
    }

    @Override
    public void readSpawnData(ByteBuf data) {
    	DataInputStream byteStream = null;
        try {
			byteStream = new DataInputStream(new ByteBufInputStream(data));
            String name = byteStream.readUTF();
            if (!name.equals(""))
                setMinecartName(name);
            model = byteStream.readUTF();
        } catch (IOException ex) {
        } finally {
        	if (byteStream != null)
        		try {
        			byteStream.close();
        		} catch (Exception e) {}
        }
    }
    
    /** Interface methods of IFluidCart */

    @Override
    public boolean canPassFluidRequests(Fluid fluid) {
    	// TODO Passing Fluids should only be possible to locomotives or t2 locomotives
    	return Fluids.WATER.is(fluid);
    }

    @Override
    public boolean canAcceptPushedFluid(EntityMinecart requester, Fluid fluid) {
    	return Fluids.WATER.is(fluid);
    }

    @Override
    public boolean canProvidePulledFluid(EntityMinecart requester, Fluid fluid) {
        return false; // TODO check for loco and tank cart if implementation is right
    }
    
    @Override
    public void setFilling(boolean fill) {
        dataWatcher.updateObject(FILLING_DATA_ID, Byte.valueOf(fill ? 1 : (byte) 0));
    }

    /** Interface methods of IFluidHandler */
    
    @Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        return tankWater.fill(resource, doFill);
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if (resource == null)
            return null;
        if (tankWater.getFluidType() == resource.getFluid())
            return tankWater.drain(resource.amount, doDrain);
        return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        return tankWater.drain(maxDrain, doDrain);
	}
	
	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid) {
		return Fluids.WATER.is(fluid);
	}
    
	@Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return false;
    }
	
    @Override
    public FluidTankInfo[] getTankInfo(ForgeDirection direction) {
        return tankManager.getTankInfo();
    }
    
    /** Interface methods of IGuiReturnHandler */
    
    @Override
	public World getWorld() {
		return worldObj;
	}
    
	@Override
    public void writeGuiData(DataOutputStream data) throws IOException {
        data.writeByte(lockController.getCurrentState());
    }

    @Override
    public void readGuiData(DataInputStream data, EntityPlayer sender) throws IOException {
        byte lock = data.readByte();
        if (PlayerPlugin.isOwnerOrOp(getOwner(), sender.getGameProfile()))
            lockController.setCurrentState(lock);
    }
    
    /** Interface methods of ILinkableCart */
    
    @Override
    public boolean isLinkable() {
        return true;
    }
    
    @Override
    public boolean canLinkWithCart(EntityMinecart cart) {
        if (cart instanceof EntityLocomotive)
            return true;

        LinkageManager lm = LinkageManager.instance();

        EntityMinecart linkA = lm.getLinkedCartA(this);
        if (linkA != null && !(linkA instanceof EntityLocomotive))
            return false;

        EntityMinecart linkB = lm.getLinkedCartB(this);
        return linkB == null || linkB instanceof EntityLocomotive;
    }

    @Override
    public boolean hasTwoLinks() {
        return true;
    }

    @Override
    public float getLinkageDistance(EntityMinecart cart) {
        return LinkageManager.LINKAGE_DISTANCE;
    }

    @Override
    public float getOptimalDistance(EntityMinecart cart) {
        return 0.9f;
    }

    @Override
    public boolean canBeAdjusted(EntityMinecart cart) {
        return true;
    }

    @Override
    public void onLinkCreated(EntityMinecart cart) {
    }

    @Override
    public void onLinkBroken(EntityMinecart cart) {
    }
    
    /** Interface methods of IMinecart */
    
    @Override
    public boolean doesCartMatchFilter(ItemStack stack, EntityMinecart cart) {
        return EnumCart.getCartType(stack) == getCartType();
    }

    /** Interface methods of IRoutableCart */
    
    @Override 
    public String getDestination() {
        return "";
    }
    
	@Override
	public boolean setDestination(ItemStack ticket) {
		return false;
	}
	
    @Override
    public GameProfile getOwner() {
        return CartTools.getCartOwner(this);
    }
    
    /** Interface methods of ISecure */    
    
    @Override
    public MultiButtonController<LockButtonState> getLockController() {
        return lockController;
    }
    
    @Override
    public boolean isSecure() {
        return getSecurityState() == LockButtonState.LOCKED || isPrivate();
    }
    
    @Override
    public String getName() {
        return LocalizationPlugin.translate(getLocalizationTag());
    }

    @Override
    public String getLocalizationTag() {
        return getCartType().getTag();
    }
    	

    




    /*public void setDestString(String dest) {}*/
 
    @Override
    public ItemStack getCartItem() {
        ItemStack item = super.getCartItem();
        if (isSecure() && CartTools.doesCartHaveOwner(this))
            ItemLocomotive.setOwnerData(item, CartTools.getCartOwner(this));
        return item;
    }
    
    /* public Methods */
    
    public LockButtonState getSecurityState() {
        return lockController.getButtonState();
    }

    public void setSecurityState(LockButtonState state) {
        lockController.setCurrentState(state);
    }
    
    public boolean isPrivate() {
        return getSecurityState() == LockButtonState.PRIVATE;
    }
    
    public boolean canControl(GameProfile user) {
        if (!isPrivate())
            return true;
        return PlayerPlugin.isOwnerOrOp(getOwner(), user);
    }

    @Override
    public void setDead() {
        super.setDead();
        InvTools.dropInventory(invWaterInput, worldObj, (int) posX, (int) posY, (int) posZ);
    }
       
    @Override
    public boolean canBeRidden() {
        return true;
    }

    @Override
    public int getSizeInventory() {
        return 0;
    }

    @Override
    public boolean isPoweredCart() {
        return false;
    }
    
    public abstract TenderRenderType getRenderType();

    
    protected abstract ItemStack getCartItemBase();
    

    protected abstract void openGui(EntityPlayer player);
    
    @Nonnull
    protected abstract EnumGui getGuiType();
    
    @Override
	public boolean doInteract(EntityPlayer player) {
        if (Game.isHost(getWorld())) {
            //ItemStack current = player.getCurrentEquippedItem();
            if (this instanceof IFluidHandler && FluidHelper.handleRightClick((IFluidHandler) this, ForgeDirection.UNKNOWN, player, true, false))
                return true;
            if (!isPrivate() || PlayerPlugin.isOwnerOrOp(getOwner(), player.getGameProfile()))
                openGui(player);
        }
        return true;
	}

	@Override
	public abstract ICartType getCartType();
    public IInventory getInvLiquids() {
        return invWaterInput;
    }

    @Override
    public boolean isItemValidForSlot(int slot, @Nullable ItemStack stack) {
        return slot == SLOT_LIQUID_INPUT && FluidItemHelper.isContainer(stack);
    }
    
    public boolean canInsertItem(int slot, ItemStack stack, EnumFacing side) {
        return isItemValidForSlot(slot, stack);
    }

    public boolean canExtractItem(int slot, ItemStack stack, EnumFacing side) {
        return slot == SLOT_LIQUID_OUTPUT;
    }


    public enum LockButtonState implements IMultiButtonState {

        UNLOCKED(new ButtonTextureSet(224, 0, 16, 16)),
        LOCKED(new ButtonTextureSet(240, 0, 16, 16)),
        PRIVATE(new ButtonTextureSet(240, 48, 16, 16));
        public static final LockButtonState[] VALUES = values();
        private final IButtonTextureSet texture;

        private LockButtonState(IButtonTextureSet texture) {
            this.texture = texture;
        }

        @Override
        public String getLabel() {
            return "";
        }

        @Override
        public IButtonTextureSet getTextureSet() {
            return texture;
        }

        @Override
        public ToolTip getToolTip() {
            return null;
        }

    }

}