/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info

 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.common.carts;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import mods.railcraft.api.carts.tender.TenderRenderType;
import mods.railcraft.common.carts.ICartType;
import mods.railcraft.common.core.RailcraftConfig;
import mods.railcraft.common.fluids.FluidHelper;
import mods.railcraft.common.fluids.FluidItemHelper;
import mods.railcraft.common.fluids.Fluids;
import mods.railcraft_loco.common.gui.EnumGui;
import mods.railcraft_loco.common.gui.GuiHandler;
import mods.railcraft.common.util.inventory.InvTools;
import mods.railcraft.common.util.inventory.PhantomInventory;
import mods.railcraft.common.util.misc.Game;
import mods.railcraft.common.util.misc.MiscTools;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class EntityTenderSolid extends EntityTender implements ISidedInventory {
    int update = MiscTools.RANDOM.nextInt();
	private int[] SLOTS;

    public EntityTenderSolid(World world) {
        super(world);
    }

    public EntityTenderSolid(World world, double d, double d1, double d2) {
    	super(world, d, d1, d2);
    }

    @Override
    public ICartType getCartType() {
        return EnumCart.TENDER_SOLID;
    }

    @Override
    public List<ItemStack> getItemsDropped() {
        List<ItemStack> items = new ArrayList<ItemStack>();
        if (RailcraftConfig.doCartsBreakOnDrop()) {
            items.add(new ItemStack(Items.minecart));
            items.add(new ItemStack(Blocks.chest));
        } else
            items.add(getCartItem());
        return items;
    }


    @Override
    public void onUpdate() {
        super.onUpdate();

        if (Game.isHost(worldObj)) {
        	
        }

//        FluidStack fluidStack = tankWater.getFluid();
//        if (!Fluids.areIdentical(fluidStack, getFluidStack()))
//            setFluidStack(fluidStack);
//
//
//        ItemStack topSlot = invLiquids.getStackInSlot(SLOT_LIQUID_INPUT);
//        if (topSlot != null && !FluidItemHelper.isContainer(topSlot)) {
//            invLiquids.setInventorySlotContents(SLOT_LIQUID_INPUT, null);
//            entityDropItem(topSlot, 1);
//        }
//
//        ItemStack bottomSlot = invLiquids.getStackInSlot(SLOT_OUTPUT);
//        if (bottomSlot != null && !FluidItemHelper.isContainer(bottomSlot)) {
//            invLiquids.setInventorySlotContents(SLOT_OUTPUT, null);
//            entityDropItem(bottomSlot, 1);
//        }

        //FIXME
//        if (update % FluidTools.BUCKET_FILL_TIME == 0)
//            FluidTools.processContainers(tank, invLiquids, SLOT_INPUT, SLOT_OUTPUT);
    }

    @Override
    public int getSizeInventory() {
        return 2;
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack stack, EnumFacing side) {
        return isItemValidForSlot(slot, stack);
    }

    @Nonnull
    @Override
    protected EnumGui getGuiType() {
        return EnumGui.TENDER_SOLID;
    }

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
        return SLOTS;
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side) {
        return isItemValidForSlot(slot, stack);
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack stack, int side) {
        return false;
	}

	@Override
	protected ItemStack getCartItemBase() {
		return EnumCart.TENDER_SOLID.getCartItem();
	}

	@Override
	protected void openGui(EntityPlayer player) {
		GuiHandler.openGui(EnumGui.TENDER_SOLID, player, worldObj, this);
	}

	@Override
	public TenderRenderType getRenderType() {
		return TenderRenderType.SOLID;
	}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack) {
        switch (slot) {
//            case SLOT_BURN:
//            case SLOT_FUEL_A:
//            case SLOT_FUEL_B:
//            case SLOT_FUEL_C:
//                return StackFilter.FUEL.matches(stack);
            case SLOT_LIQUID_INPUT:
                FluidStack fluidStack = FluidItemHelper.getFluidStackInContainer(stack);
                if (fluidStack != null && fluidStack.amount > FluidHelper.BUCKET_VOLUME)
                    return false;
                return FluidItemHelper.containsFluid(stack, Fluids.WATER.get(1));
//            case SLOT_TICKET:
//                return ItemTicket.FILTER.matches(stack);
            default:
                return false;
        }
    }
}
