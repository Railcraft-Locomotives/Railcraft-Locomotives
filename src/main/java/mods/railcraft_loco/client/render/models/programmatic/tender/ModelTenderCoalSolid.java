/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info

 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.client.render.models.programmatic.tender;

import mods.railcraft.client.render.models.ModelSimple;
import net.minecraft.client.model.ModelRenderer;

public class ModelTenderCoalSolid extends ModelSimple {

    public ModelTenderCoalSolid() {
        super("tender");

        renderer.setTextureSize(128, 64);

        setTextureOffset("tender.wheels", 1, 23);
        setTextureOffset("tender.frame", 1, 1);
        setTextureOffset("tender.crate", 67, 38);
        //setTextureOffset("tender.cab", 81, 8);
        //setTextureOffset("tender.cowcatcher", 1, 43);
        //setTextureOffset("tender.stack", 49, 43);
        //setTextureOffset("tender.dome", 23, 43);

        renderer.rotationPointX = 8F;
        renderer.rotationPointY = 8F;
        renderer.rotationPointZ = 8F;

        ModelRenderer tender = renderer;
        tender.addBox("wheels", -20F, -5F, -16F, 23, 2, 16);
        tender.addBox("frame", -21F, -7F, -17F, 25, 2, 18);
        tender.addBox("crate", -21F, -7F, -18F, 25, 11, 18);
        //tender.addBox("cab", -4F, -19F, -16F, 7, 12, 16);
        //tender.addBox("cowcatcher", -22F, -8F, -14F, 3, 5, 12);
        //tender.addBox("stack", -17F, -24F, -10F, 4, 6, 4);
        //tender.addBox("dome", -11F, -20F, -11F, 6, 2, 6);

    }

}
