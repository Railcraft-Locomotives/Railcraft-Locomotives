/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2017
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.client.gui;

import mods.railcraft.client.gui.EntityGui;
import mods.railcraft.client.gui.GuiTools;
import mods.railcraft.client.gui.buttons.GuiBetterButton;
import mods.railcraft.client.gui.buttons.GuiMultiButton;
import mods.railcraft.client.gui.buttons.GuiToggleButtonSmall;
import mods.railcraft_loco.common.carts.EntityTender;
import mods.railcraft.common.carts.EntityLocomotive.LocoMode;
import mods.railcraft.common.carts.EntityLocomotive.LocoSpeed;
import mods.railcraft.common.core.RailcraftConstants;
import mods.railcraft_loco.common.gui.containers.ContainerTender;
import mods.railcraft.common.gui.tooltips.ToolTip;
import mods.railcraft.common.plugins.forge.LocalizationPlugin;
import mods.railcraft.common.util.network.PacketBuilder;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.StatCollector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class GuiTender extends EntityGui {

    private final EntityTender tender;
    private final EntityPlayer player;
    private final String typeTag;
    private GuiMultiButton lockButton;
    private ToolTip lockedToolTips;
    private ToolTip unlockedToolTips;
    private ToolTip privateToolTips;
    private String tenderOwner;

    protected GuiTender(InventoryPlayer inv, EntityTender tender, ContainerTender container, String typeTag, String guiName, int guiHeight, boolean hasIdleMode) {
        super(tender, container, RailcraftConstants.GUI_TEXTURE_FOLDER + guiName);
        ySize = guiHeight;
        this.tender = tender;
        this.player = inv.player;
        this.typeTag = typeTag;
        lockedToolTips = ToolTip.buildToolTip("gui.railcraft_loco.tender.tips.button.locked", "{owner}=[Unknown]");
        unlockedToolTips = ToolTip.buildToolTip("gui.railcraft_loco.tender.tips.button.unlocked", "{owner}=[Unknown]");
        privateToolTips = ToolTip.buildToolTip("gui.railcraft_loco.tender.tips.button.private", "{owner}=[Unknown]");
    }

    @Override
    public void initGui() {
        super.initGui();
        if (tender == null)
            return;
        buttonList.clear();
        int w = (width - xSize) / 2;
        int h = (height - ySize) / 2;

        buttonList.add(lockButton = new GuiMultiButton(8, w + 152, h + ySize - 111, 16, tender.getLockController()));
        lockButton.enabled = tender.clientCanLock;
    }

    @Override
    protected void actionPerformed(GuiButton guibutton) {
        if (tender == null)
            return;
        super.actionPerformed(guibutton);
        updateButtons();
        sendUpdatePacket();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        updateButtons();
    }

    private void updateButtons() {
        lockButton.enabled = tender.clientCanLock;
        lockButton.setToolTip(tender.isPrivate() ? privateToolTips : tender.isSecure() ? lockedToolTips : lockButton.enabled ? unlockedToolTips : null);
        String ownerName = ((ContainerTender) container).ownerName;
        if (ownerName != null && !ownerName.equals(tenderOwner)) {
        	tenderOwner = ownerName;
            lockedToolTips = ToolTip.buildToolTip("railcraft_loco.gui.tender.tips.button.locked", "{owner}=" + ownerName);
            unlockedToolTips = ToolTip.buildToolTip("railcraft_loco_gui.tender.tips.button.unlocked", "{owner}=" + ownerName);
            privateToolTips = ToolTip.buildToolTip("railcraft_loco.gui.tender.tips.button.private", "{owner}=" + ownerName);
        }
    }

    @Override
    public void onGuiClosed() {
        sendUpdatePacket();
    }

    private void sendUpdatePacket() {
        PacketBuilder.instance().sendGuiReturnPacket(tender);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String name = tender.getName();
        GuiTools.drawCenteredString(fontRendererObj, name, 6);
        fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, (ySize - 96) + 2, 0x404040);
    }

}