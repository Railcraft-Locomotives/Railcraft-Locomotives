/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2017
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.client.gui;

import org.apache.logging.log4j.Level;

import mods.railcraft.common.util.misc.Game;
import mods.railcraft_loco.common.carts.EntityTenderSolid;
import mods.railcraft_loco.common.gui.EnumGui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.world.World;

/**
 * @author CovertJaguar <http://www.railcraft.info>
 */
public class FactoryGui {

    public static GuiScreen build(EnumGui gui, InventoryPlayer inv, Object obj, World world, int x, int y, int z) {
        try {
            switch (gui) {
                case TENDER_DUST:
                    return new GuiTenderSolid(inv, (EntityTenderSolid) obj);
                case TENDER_LIQUID:
                    return new GuiTenderSolid(inv, (EntityTenderSolid) obj);
                case TENDER_SOLID:
                    return new GuiTenderSolid(inv, (EntityTenderSolid) obj);
                default:
                    //TODO: Fix this
//                    return RailcraftModuleManager.getGuiScreen(gui, inv, obj, world, x, y, z);
            }
        } catch (ClassCastException ex) {
            Game.log(Level.WARN, "Error when attempting to build gui {0}: {1}", gui, ex);
        }
        return null;
    }

}