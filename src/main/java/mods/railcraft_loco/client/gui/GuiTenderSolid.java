/*------------------------------------------------------------------------------
 Copyright (c) CovertJaguar, 2011-2016
 http://railcraft.info
 This code is the property of CovertJaguar
 and may only be used with explicit written
 permission unless otherwise specified on the
 license page at http://railcraft.info/wiki/info:license.
 -----------------------------------------------------------------------------*/
package mods.railcraft_loco.client.gui;

import mods.railcraft_loco.common.carts.EntityTenderSolid;
import mods.railcraft_loco.common.gui.containers.ContainerTenderSolid;
import net.minecraft.entity.player.InventoryPlayer;

public class GuiTenderSolid extends GuiTender {

    private final EntityTenderSolid tender;

    public GuiTenderSolid(InventoryPlayer inv, EntityTenderSolid tender) {
        super(inv, tender, ContainerTenderSolid.make(inv, tender), "steam", "gui_tender_solid.png", 205, true);
        this.tender = tender;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
        super.drawGuiContainerBackgroundLayer(par1, par3, par3);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
    }

}